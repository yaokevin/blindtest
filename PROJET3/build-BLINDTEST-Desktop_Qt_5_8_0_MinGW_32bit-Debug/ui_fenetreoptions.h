/********************************************************************************
** Form generated from reading UI file 'fenetreoptions.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETREOPTIONS_H
#define UI_FENETREOPTIONS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FenetreOptions
{
public:
    QPushButton *BtnAccueil;
    QLabel *labelNiveau;
    QRadioButton *RBniveau1;
    QRadioButton *RBniveau2;
    QRadioButton *RBniveau3;

    void setupUi(QWidget *FenetreOptions)
    {
        if (FenetreOptions->objectName().isEmpty())
            FenetreOptions->setObjectName(QStringLiteral("FenetreOptions"));
        FenetreOptions->resize(750, 628);
        BtnAccueil = new QPushButton(FenetreOptions);
        BtnAccueil->setObjectName(QStringLiteral("BtnAccueil"));
        BtnAccueil->setGeometry(QRect(0, 0, 71, 51));
        QIcon icon;
        icon.addFile(QStringLiteral("../build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug/Images/Home.png"), QSize(), QIcon::Normal, QIcon::Off);
        BtnAccueil->setIcon(icon);
        BtnAccueil->setIconSize(QSize(32, 32));
        labelNiveau = new QLabel(FenetreOptions);
        labelNiveau->setObjectName(QStringLiteral("labelNiveau"));
        labelNiveau->setGeometry(QRect(200, 100, 351, 71));
        labelNiveau->setStyleSheet(QStringLiteral("font: 24pt \"MS Shell Dlg 2\";"));
        RBniveau1 = new QRadioButton(FenetreOptions);
        RBniveau1->setObjectName(QStringLiteral("RBniveau1"));
        RBniveau1->setGeometry(QRect(290, 200, 171, 31));
        RBniveau1->setStyleSheet(QStringLiteral("font: 16pt \"MS Shell Dlg 2\";"));
        RBniveau2 = new QRadioButton(FenetreOptions);
        RBniveau2->setObjectName(QStringLiteral("RBniveau2"));
        RBniveau2->setGeometry(QRect(290, 260, 171, 31));
        RBniveau2->setStyleSheet(QStringLiteral("font: 16pt \"MS Shell Dlg 2\";"));
        RBniveau3 = new QRadioButton(FenetreOptions);
        RBniveau3->setObjectName(QStringLiteral("RBniveau3"));
        RBniveau3->setGeometry(QRect(290, 320, 171, 31));
        RBniveau3->setStyleSheet(QStringLiteral("font: 16pt \"MS Shell Dlg 2\";"));

        retranslateUi(FenetreOptions);

        QMetaObject::connectSlotsByName(FenetreOptions);
    } // setupUi

    void retranslateUi(QWidget *FenetreOptions)
    {
        FenetreOptions->setWindowTitle(QApplication::translate("FenetreOptions", "Form", Q_NULLPTR));
        BtnAccueil->setText(QString());
        labelNiveau->setText(QApplication::translate("FenetreOptions", "Niveau de difficult\303\251s", Q_NULLPTR));
        RBniveau1->setText(QApplication::translate("FenetreOptions", "Niveau 1", Q_NULLPTR));
        RBniveau2->setText(QApplication::translate("FenetreOptions", "Niveau 2", Q_NULLPTR));
        RBniveau3->setText(QApplication::translate("FenetreOptions", "Niveau 3", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class FenetreOptions: public Ui_FenetreOptions {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETREOPTIONS_H
