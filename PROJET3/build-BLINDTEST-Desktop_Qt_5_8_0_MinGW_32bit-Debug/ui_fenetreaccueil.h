/********************************************************************************
** Form generated from reading UI file 'fenetreaccueil.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETREACCUEIL_H
#define UI_FENETREACCUEIL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FenetreAccueil
{
public:
    QPushButton *BtnOptions;
    QPushButton *BtnRegles;
    QPushButton *BtnJouer;
    QPushButton *BtnScores;

    void setupUi(QWidget *FenetreAccueil)
    {
        if (FenetreAccueil->objectName().isEmpty())
            FenetreAccueil->setObjectName(QStringLiteral("FenetreAccueil"));
        FenetreAccueil->resize(1041, 632);
        BtnOptions = new QPushButton(FenetreAccueil);
        BtnOptions->setObjectName(QStringLiteral("BtnOptions"));
        BtnOptions->setGeometry(QRect(420, 250, 351, 201));
        QFont font;
        font.setFamily(QStringLiteral("Onyx"));
        font.setPointSize(36);
        BtnOptions->setFont(font);
        BtnOptions->setCursor(QCursor(Qt::PointingHandCursor));
        BtnRegles = new QPushButton(FenetreAccueil);
        BtnRegles->setObjectName(QStringLiteral("BtnRegles"));
        BtnRegles->setGeometry(QRect(60, 250, 351, 201));
        BtnRegles->setFont(font);
        BtnRegles->setCursor(QCursor(Qt::PointingHandCursor));
        BtnJouer = new QPushButton(FenetreAccueil);
        BtnJouer->setObjectName(QStringLiteral("BtnJouer"));
        BtnJouer->setGeometry(QRect(60, 40, 351, 201));
        BtnJouer->setFont(font);
        BtnJouer->setCursor(QCursor(Qt::PointingHandCursor));
        BtnScores = new QPushButton(FenetreAccueil);
        BtnScores->setObjectName(QStringLiteral("BtnScores"));
        BtnScores->setGeometry(QRect(420, 40, 351, 201));
        BtnScores->setFont(font);
        BtnScores->setCursor(QCursor(Qt::PointingHandCursor));

        retranslateUi(FenetreAccueil);

        QMetaObject::connectSlotsByName(FenetreAccueil);
    } // setupUi

    void retranslateUi(QWidget *FenetreAccueil)
    {
        FenetreAccueil->setWindowTitle(QApplication::translate("FenetreAccueil", "Form", Q_NULLPTR));
        BtnOptions->setText(QApplication::translate("FenetreAccueil", "OPTIONS", Q_NULLPTR));
        BtnRegles->setText(QApplication::translate("FenetreAccueil", "REGLES", Q_NULLPTR));
        BtnJouer->setText(QApplication::translate("FenetreAccueil", "JOUER", Q_NULLPTR));
        BtnScores->setText(QApplication::translate("FenetreAccueil", "SCORES", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class FenetreAccueil: public Ui_FenetreAccueil {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETREACCUEIL_H
