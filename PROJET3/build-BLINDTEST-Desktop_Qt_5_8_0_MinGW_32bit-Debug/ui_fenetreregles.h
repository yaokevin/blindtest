/********************************************************************************
** Form generated from reading UI file 'fenetreregles.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETREREGLES_H
#define UI_FENETREREGLES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FenetreRegles
{
public:
    QPushButton *BtnAccueil;

    void setupUi(QWidget *FenetreRegles)
    {
        if (FenetreRegles->objectName().isEmpty())
            FenetreRegles->setObjectName(QStringLiteral("FenetreRegles"));
        FenetreRegles->resize(619, 448);
        BtnAccueil = new QPushButton(FenetreRegles);
        BtnAccueil->setObjectName(QStringLiteral("BtnAccueil"));
        BtnAccueil->setGeometry(QRect(0, 0, 71, 51));
        QIcon icon;
        icon.addFile(QStringLiteral("../build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug/Images/Home.png"), QSize(), QIcon::Normal, QIcon::Off);
        BtnAccueil->setIcon(icon);
        BtnAccueil->setIconSize(QSize(32, 32));

        retranslateUi(FenetreRegles);

        QMetaObject::connectSlotsByName(FenetreRegles);
    } // setupUi

    void retranslateUi(QWidget *FenetreRegles)
    {
        FenetreRegles->setWindowTitle(QApplication::translate("FenetreRegles", "Form", Q_NULLPTR));
        BtnAccueil->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class FenetreRegles: public Ui_FenetreRegles {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETREREGLES_H
