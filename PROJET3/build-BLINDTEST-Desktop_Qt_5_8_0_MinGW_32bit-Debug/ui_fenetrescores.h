/********************************************************************************
** Form generated from reading UI file 'fenetrescores.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETRESCORES_H
#define UI_FENETRESCORES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FenetreScores
{
public:
    QPushButton *BtnAccueil;

    void setupUi(QWidget *FenetreScores)
    {
        if (FenetreScores->objectName().isEmpty())
            FenetreScores->setObjectName(QStringLiteral("FenetreScores"));
        FenetreScores->resize(668, 442);
        BtnAccueil = new QPushButton(FenetreScores);
        BtnAccueil->setObjectName(QStringLiteral("BtnAccueil"));
        BtnAccueil->setGeometry(QRect(0, 0, 71, 51));
        QIcon icon;
        icon.addFile(QStringLiteral("../build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug/Images/Home.png"), QSize(), QIcon::Normal, QIcon::Off);
        BtnAccueil->setIcon(icon);
        BtnAccueil->setIconSize(QSize(32, 32));

        retranslateUi(FenetreScores);

        QMetaObject::connectSlotsByName(FenetreScores);
    } // setupUi

    void retranslateUi(QWidget *FenetreScores)
    {
        FenetreScores->setWindowTitle(QApplication::translate("FenetreScores", "Form", Q_NULLPTR));
        BtnAccueil->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class FenetreScores: public Ui_FenetreScores {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETRESCORES_H
