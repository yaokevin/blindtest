/********************************************************************************
** Form generated from reading UI file 'fenetrejouer.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FENETREJOUER_H
#define UI_FENETREJOUER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FenetreJouer
{
public:
    QPushButton *BtnAccueil;
    QPushButton *BtnPlay;
    QFrame *line_5;
    QLCDNumber *ChronoJeu;
    QPushButton *BtnReponse1;
    QPushButton *BtnReponse2;
    QPushButton *BtnReponse3;
    QPushButton *BtnReponse4;
    QFrame *line_6;
    QLabel *LabelPoints;

    void setupUi(QWidget *FenetreJouer)
    {
        if (FenetreJouer->objectName().isEmpty())
            FenetreJouer->setObjectName(QStringLiteral("FenetreJouer"));
        FenetreJouer->resize(800, 800);
        FenetreJouer->setMinimumSize(QSize(0, 0));
        BtnAccueil = new QPushButton(FenetreJouer);
        BtnAccueil->setObjectName(QStringLiteral("BtnAccueil"));
        BtnAccueil->setGeometry(QRect(0, 0, 75, 51));
        QIcon icon;
        icon.addFile(QStringLiteral("../build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug/Images/Home.png"), QSize(), QIcon::Normal, QIcon::Off);
        BtnAccueil->setIcon(icon);
        BtnAccueil->setIconSize(QSize(32, 32));
        BtnPlay = new QPushButton(FenetreJouer);
        BtnPlay->setObjectName(QStringLiteral("BtnPlay"));
        BtnPlay->setGeometry(QRect(340, 200, 111, 91));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug/Images/Play.png"), QSize(), QIcon::Normal, QIcon::Off);
        BtnPlay->setIcon(icon1);
        BtnPlay->setIconSize(QSize(64, 64));
        line_5 = new QFrame(FenetreJouer);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setGeometry(QRect(100, 330, 581, 21));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);
        ChronoJeu = new QLCDNumber(FenetreJouer);
        ChronoJeu->setObjectName(QStringLiteral("ChronoJeu"));
        ChronoJeu->setGeometry(QRect(220, 70, 351, 101));
        ChronoJeu->setSmallDecimalPoint(true);
        BtnReponse1 = new QPushButton(FenetreJouer);
        BtnReponse1->setObjectName(QStringLiteral("BtnReponse1"));
        BtnReponse1->setGeometry(QRect(150, 370, 461, 71));
        QFont font;
        font.setPointSize(16);
        BtnReponse1->setFont(font);
        BtnReponse2 = new QPushButton(FenetreJouer);
        BtnReponse2->setObjectName(QStringLiteral("BtnReponse2"));
        BtnReponse2->setGeometry(QRect(150, 450, 461, 71));
        BtnReponse2->setFont(font);
        BtnReponse3 = new QPushButton(FenetreJouer);
        BtnReponse3->setObjectName(QStringLiteral("BtnReponse3"));
        BtnReponse3->setGeometry(QRect(150, 530, 461, 71));
        BtnReponse3->setFont(font);
        BtnReponse4 = new QPushButton(FenetreJouer);
        BtnReponse4->setObjectName(QStringLiteral("BtnReponse4"));
        BtnReponse4->setGeometry(QRect(150, 610, 461, 71));
        BtnReponse4->setFont(font);
        line_6 = new QFrame(FenetreJouer);
        line_6->setObjectName(QStringLiteral("line_6"));
        line_6->setGeometry(QRect(100, 700, 581, 21));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);
        LabelPoints = new QLabel(FenetreJouer);
        LabelPoints->setObjectName(QStringLiteral("LabelPoints"));
        LabelPoints->setGeometry(QRect(230, 720, 291, 51));
        LabelPoints->setStyleSheet(QLatin1String("background-color: rgb(108, 104, 113);\n"
"color: rgb(255, 255, 255);\n"
"font: 20pt \"MS Shell Dlg 2\";"));
        LabelPoints->setScaledContents(false);
        LabelPoints->setAlignment(Qt::AlignCenter);
        LabelPoints->setWordWrap(false);

        retranslateUi(FenetreJouer);

        QMetaObject::connectSlotsByName(FenetreJouer);
    } // setupUi

    void retranslateUi(QWidget *FenetreJouer)
    {
        FenetreJouer->setWindowTitle(QApplication::translate("FenetreJouer", "Form", Q_NULLPTR));
        BtnAccueil->setText(QString());
        BtnPlay->setText(QString());
        BtnReponse1->setText(QApplication::translate("FenetreJouer", "Reponse 1", Q_NULLPTR));
        BtnReponse2->setText(QApplication::translate("FenetreJouer", "Reponse 2", Q_NULLPTR));
        BtnReponse3->setText(QApplication::translate("FenetreJouer", "Reponse 3", Q_NULLPTR));
        BtnReponse4->setText(QApplication::translate("FenetreJouer", "Reponse 4", Q_NULLPTR));
        LabelPoints->setText(QApplication::translate("FenetreJouer", "SCORES", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class FenetreJouer: public Ui_FenetreJouer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FENETREJOUER_H
