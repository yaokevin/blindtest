#include "fenetreregles.h"
#include "ui_fenetreregles.h"
#include "fenetreaccueil.h"

FenetreRegles::FenetreRegles(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FenetreRegles)
{
    ui->setupUi(this);
}

FenetreRegles::~FenetreRegles()
{
    delete ui;
}

void FenetreRegles::on_BtnAccueil_clicked()
{
    FenetreAccueil *fenetreAccueil = new FenetreAccueil();
    fenetreAccueil->setFixedSize(800, 800);
    fenetreAccueil->PositionnerBtn();
    fenetreAccueil->show();
    close();
}
