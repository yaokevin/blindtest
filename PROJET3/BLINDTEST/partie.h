#ifndef PARTIE_H
#define PARTIE_H


#include "niveau.h"
#include <QSettings>
#include <QDirIterator>
#include <QVector>
#include <QDebug>

class FenetreJouer;
class Joueur;
class Quiz;

class Partie
{
public:
    Partie(Joueur *monJoueur, FenetreJouer *fenJeu);

    void Random10Extraits(); //Charge aléatoirement 10 extraits dans le dossier du niveau
    void NomTitre10Extraits(); //recuperation du Nomtitre en BDD
    void LancerPartie();
    void ouvrirDossier();
    int Score;
    FenetreJouer * getFenJeu();

private:
    Joueur *monJoueur;
    QVector<Quiz*> * monTabQuiz;

    FenetreJouer * fenJeu;

};

#endif // PARTIE_H
