#ifndef DAL_H
#define DAL_H

#include "fenetrescores.h"
#include "partie.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <iostream>

class DAL
{
public:
    DAL();
    void createDbTables(); //creation table necessaire (JOUEUR & EXTRAITS)
    void insertScore(QString value); //insertion score joueur dans BDD
    void insertChoixJoueur(QString value); //insertion réponse joueur dans BDD
    void selectScores(); //selections de tout les scores dans BDD
    void updateParam(QString param, QString value);

private:
    QSqlDatabase dbmgr;
};

#endif // DAL_H
