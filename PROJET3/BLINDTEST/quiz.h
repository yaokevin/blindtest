#ifndef QUIZ_H
#define QUIZ_H

#include <QWidget>
#include <time.h>
#include <QTimer>
#include <QTime>
#include <QLCDNumber>
#include "fenetrejouer.h"
#include "partie.h"

class Quiz : public QWidget
{
    Q_OBJECT

public:
    Quiz();
    Quiz(QString monExtraits, Partie * maPartie);
    void ChoixExtrait(); //random choix extrait
    void afficheTitreExtrait(); //affichage bonne réponse
    void choix3FaussesReponses(); //methode initialisation des fausses réponses
    void lancerQuiz();

    QTimer *tempsMax;
    QTimer *delaisAvantRefresh;
    QTime * tempsEcoule;


public slots:
    void ChronoStart();
    void finDuChrono();
    void changementChrono();

private:
     QString titreExtraitQuiz;
     float valeurChronoQuiz;
     int pointQuiz;
     QString FausseReponsesQuiz[3];
     double compteur;
     Partie * maPartie;
};

#endif // QUIZ_H
