#include "quiz.h"
#include <QFile>
#include <QTextStream>
#include <algorithm>

Quiz::Quiz()
{

}

Quiz::Quiz(QString monExtraits, Partie *maPartie)
{
    this->maPartie = maPartie;
    this->titreExtraitQuiz=monExtraits;
}

void Quiz::lancerQuiz()
{
    tempsEcoule = new QTime();
    tempsEcoule->start();

    delaisAvantRefresh = new QTimer();
    delaisAvantRefresh->setInterval(150);
    delaisAvantRefresh->start();

    tempsMax = new QTimer();
    tempsMax->setInterval(30000);
    maPartie->getFenJeu()->chronoJeu->display(compteur);
    tempsMax->start();

    titreExtraitQuiz = titreExtraitQuiz;
    valeurChronoQuiz = 30;
    this->choix3FaussesReponses();
    pointQuiz = 0;
    QObject::connect(Quiz::tempsMax, SIGNAL(timeout()), this, SLOT(finDuChrono()));
    QObject::connect(Quiz::delaisAvantRefresh, SIGNAL(timeout()), this, SLOT(changementChrono()));

    compteur = 30;
}


void Quiz::choix3FaussesReponses(){

    QString texte1, texte2, texte3, texte4;
    QFile fichier("./Niveau/ListeExtraits.txt");

    if(fichier.open(QIODevice::ReadOnly | QIODevice::Text))
    {
         FausseReponsesQuiz[0] = fichier.readLine(0);
         FausseReponsesQuiz[1]  = fichier.readLine();
         FausseReponsesQuiz[2]  = fichier.readLine();
         /*ui->BtnReponse1->setText(FausseReponsesQuiz[0] );
         ui->BtnReponse2->setText(FausseReponsesQuiz[0] );
         ui->BtnReponse3->setText(FausseReponsesQuiz[0] );*/
         fichier.close();
    }

}

void Quiz::ChronoStart(){
    compteur -= 0.001;
    tempsMax->setInterval(30000);
    maPartie->getFenJeu()->chronoJeu->display(compteur);
    if(compteur==0){
        delete tempsMax;
    }
}

void Quiz::finDuChrono(){
    delaisAvantRefresh->blockSignals(true);
    compteur = 0;
    maPartie->getFenJeu()->chronoJeu->display(QString().setNum(0.0, 'f', 3));
}

void Quiz::changementChrono(){
    compteur = 30.000-tempsEcoule->elapsed()/1000.0;
    maPartie->getFenJeu()->chronoJeu->display(QString().setNum(compteur, 'f', 3));

}
