#ifndef FENETRESCORES_H
#define FENETRESCORES_H

#include <QWidget>
#include "dal.h"

class FenetreAccueil;

namespace Ui {
class FenetreScores;
}

class FenetreScores : public QWidget
{
    Q_OBJECT

public:
    explicit FenetreScores(QWidget *parent = 0);
    ~FenetreScores();

    void afficheScores(); //Recuperation dans BDD de tout les scores et affichage en tableau


private slots:
    void on_BtnAccueil_clicked(); //Retour a l'accueil

private:
    Ui::FenetreScores *ui;
};

#endif // FENETRESCORES_H
