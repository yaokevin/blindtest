#ifndef JOUEUR_H
#define JOUEUR_H

#include "choix.h"


class Joueur
{
public:
    Joueur();
    void setPointsJoueur();
    void getPointsJoueur();
    void getScore();
    void setScore();

private:
    std::string NomJoueur;
    int Score;

};

#endif // JOUEUR_H
