#include "fenetreoptions.h"
#include "ui_fenetreoptions.h"
#include "fenetreaccueil.h"
#include "fenetrejouer.h"
#include <QSettings>
#include <iostream>
using namespace std;

FenetreOptions::FenetreOptions(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FenetreOptions)
{
    ui->setupUi(this);
    load_saved_config();

}

FenetreOptions::~FenetreOptions()
{
    delete ui;
}

void FenetreOptions::load_saved_config()
{

    QSettings settings("Kevin-Dev-Kikou", "Blindtest");
    QString url = settings.value("niveau").toString();

    QString match = QString("Niveau1");
    if (url.contains(match)){
        ui->RBniveau1->setChecked(true);
    }

    match = QString("Niveau2");
    if (url.contains(match)){
        ui->RBniveau2->setChecked(true);
    }

    match = QString("Niveau3");
    if (url.contains(match)){
        ui->RBniveau3->setChecked(true);
    }

}

void FenetreOptions::on_BtnAccueil_clicked()
{
    FenetreAccueil *fenetreAccueil = new FenetreAccueil();
    fenetreAccueil->setFixedSize(800, 800);
    fenetreAccueil->PositionnerBtn();
    fenetreAccueil->show();
    hide();
}



void FenetreOptions::on_RBniveau1_clicked()
{
    /*FenetreJouer *fenetreJouer = new FenetreJouer;
    fenetreJouer->player->setMedia(QUrl::fromLocalFile("Z:\\PROJET3\\build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug\\Niveau\\Niveau1"));
    fenetreJouer->player->setVolume(50);
    fenetreJouer->player->play();*/

    QSettings settings("Kevin-Dev-Kikou", "Blindtest");
    settings.setValue("niveau", "Z:\\PROJET3\\build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug\\Niveau\\Niveau1");

}

void FenetreOptions::on_RBniveau2_clicked()
{
    /* FenetreJouer *fenetreJouer = new FenetreJouer;
     fenetreJouer->player->setMedia(QUrl::fromLocalFile("Z:\\PROJET3\\build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug\\Niveau\\Niveau2"));
     fenetreJouer->player->setVolume(50);
     fenetreJouer->player->play();*/

     QSettings settings("Kevin-Dev-Kikou", "Blindtest");
     settings.setValue("niveau", "Z:\\PROJET3\\build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug\\Niveau\\Niveau2");


}

void FenetreOptions::on_RBniveau3_clicked()
{
    /*FenetreJouer *fenetreJouer = new FenetreJouer;
    fenetreJouer->player->setMedia(QUrl::fromLocalFile("Z:\\PROJET3\\build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug\\Niveau\\Niveau3"));
    fenetreJouer->player->setVolume(50);
    fenetreJouer->player->play();*/
    QSettings settings("Kevin-Dev-Kikou", "Blindtest");
    settings.setValue("niveau", "Z:\\PROJET3\\build-BLINDTEST-Desktop_Qt_5_8_0_MinGW_32bit-Debug\\Niveau\\Niveau3");

}
