#include "fenetrejouer.h"
#include "ui_fenetrejouer.h"
#include "fenetreaccueil.h"

#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QDeadlineTimer>
#include <QSettings>
#include <QDirIterator>
#include <QApplication>



FenetreJouer::FenetreJouer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FenetreJouer)
{
    ui->setupUi(this);
    this->afficheReponses();

    chronoJeu = ui->ChronoJeu;

    Joueur *monJoueur = new Joueur();
    maPartie = new Partie(monJoueur, this);

    //chronoJeu->setDigitCount(6);
    //chronoJeu->setSmallDecimalPoint(true);

}

FenetreJouer::~FenetreJouer()
{
    delete ui;
}


void FenetreJouer::on_BtnAccueil_clicked() //Passage de l'interface de Jeu à l'accueil
{
    FenetreAccueil *fenetreAccueil = new FenetreAccueil();
    fenetreAccueil->setFixedSize(800, 800);
    fenetreAccueil->PositionnerBtn();
    fenetreAccueil->show();
    close();
}

void FenetreJouer::on_BtnPlay_clicked() //Initialisation du player
{

    maPartie->LancerPartie();

}

void FenetreJouer::on_BtnReponse1_clicked()
{


}

void FenetreJouer::on_BtnReponse2_clicked()
{

}

void FenetreJouer::on_BtnReponse3_clicked()
{

}

void FenetreJouer::on_BtnReponse4_clicked()
{

}

void FenetreJouer::afficheReponses()
{
}





