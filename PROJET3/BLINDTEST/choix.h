#ifndef CHOIX_H
#define CHOIX_H

#include <QWidget>
#include "dal.h"

class Choix
{
public:
    Choix();
    void getChoixJoueur(); //récuperer le choix du joueur
    void setChoixJoueur(); //prendre en compte le choix du joueur
private:

};

#endif // CHOIX_H
