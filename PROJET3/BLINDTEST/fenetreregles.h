#ifndef FENETREREGLES_H
#define FENETREREGLES_H

#include <QWidget>

class FenetreAccueil;

namespace Ui {
class FenetreRegles;
}

class FenetreRegles : public QWidget
{
    Q_OBJECT

public:
    explicit FenetreRegles(QWidget *parent = 0);
    ~FenetreRegles();

    void afficheRegle(); //affichage reglement jeu (LABEL)

private slots:
    void on_BtnAccueil_clicked(); //Retour a l'accueil

private:
    Ui::FenetreRegles *ui;
};

#endif // FENETREREGLES_H
