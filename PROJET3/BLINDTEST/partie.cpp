#include "partie.h"
#include "joueur.h"
#include "quiz.h"
#include "fenetrejouer.h"

Partie::Partie(Joueur *monJoueur, FenetreJouer * fenJeu)
{
    this->fenJeu = fenJeu;
    this->monJoueur = monJoueur;
    QStringList listeExtraits;

    monTabQuiz = new QVector<Quiz*>;

    // charger liste de 10 extrait aleatoire

    listeExtraits.append("test");

    //changer 1 en 10
    for (int i=0; i<1; i++){
        monTabQuiz->push_back(new Quiz(listeExtraits[i], this));
    }
}


void Partie::LancerPartie(){


    /*playlist = new QMediaPlaylist;
    playlist->addMedia(QUrl("http://example.com/movie1.mp4"));
    playlist->addMedia(QUrl("http://example.com/movie2.mp4"));
    playlist->addMedia(QUrl("http://example.com/movie3.mp4"));
    playlist->setCurrentIndex(1);*/

    fenJeu->player = new QMediaPlayer;
    QSettings settings("Kevin-Dev-Kikou", "Blindtest");
    QString dir = settings.value("niveau").toString(); // blablabla/niveau1/
    //qDebug() << dir;


    (*monTabQuiz)[0]->lancerQuiz();

    QDirIterator it {dir, QDir::Files,
                           QDirIterator::Subdirectories};
    while (it.hasNext()) {
        //qDebug() << it.next();
        it.next();
        qDebug() << it.filePath();

        fenJeu->player->setMedia(QUrl(it.filePath()));
        //selection morceau
        fenJeu->player->setVolume(50);
        fenJeu->player->play();
    }

}

FenetreJouer * Partie::getFenJeu()
{
    return this->fenJeu;
}
