#include "dal.h"
#include <iostream>
#include <QSqlError>
#include <QDebug>


DAL::DAL()
{
    dbmgr = QSqlDatabase::addDatabase("QSQLITE");
    dbmgr.setDatabaseName("blindtest.db");

    if (dbmgr.open()) {
        std::cout << "Database opened" << std::endl;
    } else {
        std::cout << "ERROR Database" << std::endl;
    }
}

void DAL::createDbTables() {
    QSqlQuery query;
    query.prepare("CREATE TABLE IF NOT EXISTS joueur (IdJoueur INTEGER PRIMARY KEY AUTOINCREMENT, Nom TEXT NULL, Score INT NOT NULL)");

    if (query.exec()) {
        std::cout << "Table joueur cree" << std::endl;
    } else {
        qDebug() << "Erreur table" << query.lastError();
    }

    QSqlQuery query2;
    query2.prepare("CREATE TABLE IF NOT EXISTS extrait (IdExtrait INTEGER PRIMARY KEY AUTOINCREMENT, NomExtraitMp3 TEXT NOT NULL, NomTitre TEXT NOT NULL)");
    if (query2.exec()) {
        std::cout << "Table extrait cree" << std::endl;
    } else {
        qDebug() << "Erreur table" << query2.lastError();
    }
}

void DAL::insertScore(QString Score) {
    QSqlQuery query;
    query.prepare("INSERT INTO joueur (Score) VALUES (:Score)");
    query.bindValue(":Score", Score);
    if (query.exec()) {
        std::cout << "Score inseree" << std::endl;
    } else {
        qDebug() << "Erreur insert" << query.lastError();
    }
}

void DAL::insertChoixJoueur(QString ChoixJoueur) {
    QSqlQuery query;
    query.prepare("INSERT INTO joueur (Score) VALUES (:Score)");
    query.bindValue(":Score", ChoixJoueur);
    if (query.exec()) {
        std::cout << "ChoixJoueur inseree" << std::endl;
    } else {
        qDebug() << "Erreur insert" << query.lastError();
    }
}

void DAL::selectScores() {
    QSqlQuery query;
    query.prepare("SELECT Score FROM joueur ORDER BY Score");
    query.bindValue(":Score", "toto"); //BINDVALUE???????????????????????????????
    if (query.exec()) {
        while (query.next()) {
            qDebug() << query.value("Score") << " "
                     << query.value("Score");
        }
    } else {
        qDebug() << "Erreur insert" << query.lastError();
    }
}

/*void DAL::updateParam(QString param, QString value) {
    QSqlQuery query;
    query.prepare("UPDATE joueur SET value = :value WHERE param = :param");
    query.bindValue(":value", value);
    query.bindValue(":param", param);
    if (query.exec()) {
        std::cout << "Mise a jour faite" << std::endl;
    } else {
        qDebug() << "Erreur insert" << query.lastError();
    }
}*/


