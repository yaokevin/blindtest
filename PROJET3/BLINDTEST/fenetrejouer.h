#ifndef FENETREJOUER_H
#define FENETREJOUER_H

#include <QWidget>
#include "partie.h"
#include "fenetreoptions.h"
#include "dal.h"
#include "joueur.h"

#include <QMediaPlayer>
#include <QDeadlineTimer>
#include <QLCDNumber>

class FenetreAccueil;


namespace Ui {
class FenetreJouer;
}

class FenetreJouer : public QWidget
{
    Q_OBJECT

public:
    explicit FenetreJouer(QWidget *parent = 0);
    ~FenetreJouer();

    void afficheReponses(); //Affichage Réponse 1 sur label pushbutton
    void affichePoints(); //Affichage points sur label score
    void RatioPointsChrono(); //fonction calcul point en fonction de chrono
    QMediaPlayer * player;
    QMediaPlaylist * playlist;    
    QLCDNumber * chronoJeu;
    Partie *maPartie;


private slots:
    void on_BtnAccueil_clicked(); //Retour a l'accueil et delete la partie

    void on_BtnPlay_clicked(); //selection aléatoire d'un extrait dans le vecteur Random10Extraits() de la classe partie , recuperation du Nomtitre dans vecteur NomTitre10Extraits() de la classe partie (test Nomtitre = Nomextrait) et affichage de celui ci aléatoirement dans 1 des 4 BtnReponse

    void on_BtnReponse1_clicked(); //verification si bonne réponse, si oui incrementation score et changement page quiz suivant

    void on_BtnReponse2_clicked(); //verification si bonne réponse, si oui incrementation score et changement page quiz suivant

    void on_BtnReponse3_clicked(); //verification si bonne réponse, si oui incrementation score et changement page quiz suivant

    void on_BtnReponse4_clicked(); //verification si bonne réponse, si oui incrementation score et changement page quiz suivant

private:
    Ui::FenetreJouer *ui;


};

#endif // FENETREJOUER_H
