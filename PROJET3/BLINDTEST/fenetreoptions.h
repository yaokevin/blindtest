#ifndef FENETREOPTIONS_H
#define FENETREOPTIONS_H

#include "niveau.h"
#include <QWidget>

class FenetreAccueil;
class FenetreJouer;

namespace Ui {
class FenetreOptions;
}

class FenetreOptions : public QWidget
{
    Q_OBJECT

public:
    explicit FenetreOptions(QWidget *parent = 0);
    ~FenetreOptions();

    void changerBackgroundColor(); //Changer couleur fond jeu
    void ChoixNiveau1(); //determine le chemin du dossier ou recuperer les extraits aléatoirement
    void ChoixNiveau2(); //determine le chemin du dossier ou recuperer les extraits aléatoirement
    void ChoixNiveau3(); //determine le chemin du dossier ou recuperer les extraits aléatoirement
    void load_saved_config(); // Charge la derniere selection de l'utilisateur

private slots:
    void on_BtnAccueil_clicked(); //Retour a l'accueil

    void on_RBniveau1_clicked();

    void on_RBniveau2_clicked();

    void on_RBniveau3_clicked();

private:
    Ui::FenetreOptions *ui;
};

#endif // FENETREOPTIONS_H
