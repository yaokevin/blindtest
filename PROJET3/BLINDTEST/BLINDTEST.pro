QT += widgets
QT += multimedia
QT += core
QT += core gui sql
SOURCES += \
    main.cpp \
    fenetreaccueil.cpp \
    fenetrejouer.cpp \
    fenetrescores.cpp \
    fenetreregles.cpp \
    joueur.cpp \
    partie.cpp \
    quiz.cpp \
    niveau.cpp \
    choix.cpp \
    dal.cpp \
    fenetreoptions.cpp

HEADERS += \
    fenetreaccueil.h \
    fenetrejouer.h \
    fenetrescores.h \
    fenetreregles.h \
    joueur.h \
    partie.h \
    quiz.h \
    niveau.h \
    choix.h \
    dal.h \
    fenetreoptions.h

FORMS += \
    mainwindow.ui \
    fenetreaccueil.ui \
    fenetrejouer.ui \
    fenetreoptions.ui \
    fenetreregles.ui \
    fenetrescores.ui
