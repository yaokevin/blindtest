#include "fenetreaccueil.h"
#include "ui_fenetreaccueil.h"

FenetreAccueil::FenetreAccueil(QWidget *parent) : //Constructeur
    QWidget(parent),
    ui(new Ui::FenetreAccueil)
{
    ui->setupUi(this);
}

FenetreAccueil::~FenetreAccueil() //Destructeur
{
    delete ui;
}

void FenetreAccueil::on_BtnJouer_clicked() //Passage de l'accueil a l'interface de Jeu
{
    FenetreJouer *fenetreJouer = new FenetreJouer();
    fenetreJouer->setFixedSize(800, 800);
    fenetreJouer->show();

    hide();
}

void FenetreAccueil::on_BtnScores_clicked() //Passage de l'accueil a l'interface de Scores
{
    FenetreScores *fenetreScores = new FenetreScores();
    fenetreScores->setFixedSize(800, 800);
    fenetreScores->show();
    close();
}

void FenetreAccueil::on_BtnRegles_clicked() //Passage de l'accueil a l'interface de Reglement jeu
{
    FenetreRegles *fenetreRegles = new FenetreRegles();
    fenetreRegles->setFixedSize(800, 800);
    fenetreRegles->show();
    close();
}

void FenetreAccueil::on_BtnOptions_clicked() //Passage de l'accueil a l'interface d'options
{
    FenetreOptions *fenetreOptions = new FenetreOptions();
    fenetreOptions->setFixedSize(800, 800);
    fenetreOptions->show();
    //fenetreOptions->load_saved_config();
    close();
}

void FenetreAccueil::PositionnerBtn()
{
    ui->BtnJouer->move(50,250);
    ui->BtnScores->move(400,250);
    ui->BtnRegles->move(50,450);
    ui->BtnOptions->move(400,450);
}
