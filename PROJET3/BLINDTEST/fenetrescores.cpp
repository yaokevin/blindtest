#include "fenetrescores.h"
#include "ui_fenetrescores.h"
#include "fenetreaccueil.h"

FenetreScores::FenetreScores(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FenetreScores)
{
    ui->setupUi(this);
}

FenetreScores::~FenetreScores()
{
    delete ui;
}

void FenetreScores::on_BtnAccueil_clicked()
{
    FenetreAccueil *fenetreAccueil = new FenetreAccueil();
    fenetreAccueil->setFixedSize(800, 800);
    fenetreAccueil->PositionnerBtn();
    fenetreAccueil->show();
    close();
}
