#ifndef FENETREACCUEIL_H
#define FENETREACCUEIL_H

#include <QWidget>
#include "fenetrejouer.h"
#include "fenetrescores.h"
#include "fenetreregles.h"
#include "fenetreoptions.h"

namespace Ui {
class FenetreAccueil;
}

class FenetreAccueil : public QWidget
{
    Q_OBJECT

public:
    explicit FenetreAccueil(QWidget *parent = 0);
    ~FenetreAccueil();

    void PositionnerBtn();

private slots:
    void on_BtnJouer_clicked(); //Choix Jouer sur interface accueil

    void on_BtnScores_clicked(); //Choix Scores sur interface accueil

    void on_BtnRegles_clicked(); //Choix Regles sur interface accueil

    void on_BtnOptions_clicked(); //Choix Options sur interface accueil



private:
    Ui::FenetreAccueil *ui;
};

#endif // FENETREACCUEIL_H
